# Ember iOS App README #

The Ember App designed for students to better engage with their campus.
Repository by Anthony Maina and team.

### Languages ###
Swift 3 and Objective C

### Summary ###

This iOS app allowed students to log in using their school email addresses, select their preferences and begin viewing photos and videos of events happening on their campus.
It was built a team of 3.

### Anthony Maina Contribution ###
I designed the complete app UI and all media uploading and processing (photos and videos).
I also built the notification system for students to be notified when an event begins.

### Status ###

Currently discontinued.

